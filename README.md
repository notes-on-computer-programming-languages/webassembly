# webassembly

Binary instruction format for a stack-based virtual machine. https://webassembly.org

* https://webassembly.org/getting-started/developers-guide

# Books
* *WebAssembly : the definitive guide : safe, fast, and portable code*
  **2021**
  * (fr) 2024 *WebAssembly : Le guide définitif*
* *The art of webAssembly : build secure, portable, high-performance applications*
  **2021**

# Unofficial documentation
* [*WebAssembly*](https://en.m.wikipedia.org/wiki/WebAssembly) @Wikipedia
* [*What programming language has the best compiler for WebAssembly?*
  ](https://www.quora.com/What-programming-language-has-the-best-compiler-for-WebAssembly)
* [*6 Security Risks to Consider with WebAssembly*
  ](https://www.jit.io/blog/6-security-risks-to-consider-with-webassembly)
  2024-02 Shuki Levy (Jit#)

# Hello World
* [webassembly hello world](https://google.com/search?q=webassembly+hello+world)
* [*Hello, World!*](https://rustwasm.github.io/book/game-of-life/hello-world.html)

# Run WASM binary
* https://webassembly.js.org/docs/usage.html
* https://webassembly.github.io/wabt/doc/wasm-interp.1.html
  * https://salsa.debian.org/debian/wabt

# Memory management
* [webassembly memory management
  ](https://google.com/search?q=webassembly+memory+management)
